import numpy as np
import matplotlib.pyplot as plt
import argparse
import scipy.misc
from path_patch import PolygonInteractor

class Dotter(object):
    """
    An path editor.

    Key-bindings

      't' toggle vertex markers on and off.  When vertex markers are on,
          you can move them, delete them


    """

    showverts = True
    epsilon = 5  # max pixel distance to count as a vertex hit

    def __init__(self, pts, image):

        self.fig, self.ax = plt.subplots(figsize=(20, 9))
        canvas = self.ax.figure.canvas

        self.vertices = pts
        x = [p[0] for p in self.vertices]
        y = [p[1] for p in self.vertices]

        self.ax.imshow(image)
        self.line, = self.ax.plot(x, y, marker='o', linestyle='')

        self._ind = None  # the active vert

        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)

    def pathpatch_changed(self, pathpatch):
        'this method is called whenever the pathpatchgon object is called'
        # only copy the artist props to the line (except visibility)
        # vis = self.line.get_visible()
        # plt.Artist.update_from(self.line, pathpatch)
        # self.line.set_visible(vis)  # don't use the pathpatch visibility state

    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'
        xy = np.asarray(self.vertices)
        xyt = self.ax.transData.transform(xy)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x)**2 + (yt - event.y)**2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None
        return ind

    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if not self.showverts:
            return
        if event.inaxes is None:
            return
        self._ind = self.get_ind_under_point(event)

        if event.button == 3:   # right click
            if self._ind == None:
                x, y = event.xdata, event.ydata
                self.vertices.append((x, y))
            else:
                del self.vertices[self._ind]

            vertices = np.asarray(self.vertices)
            self.line.set_data(vertices[:,0], vertices[:,1])
            self.canvas.restore_region(self.background)
            self.ax.draw_artist(self.line)
            self.canvas.blit(self.ax.bbox)
            self.canvas.draw()

    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if not self.showverts:
            return
        if event.button != 1:
            return
        self._ind = None

    def key_press_callback(self, event):
        'whenever a key is pressed'
        if not event.inaxes:
            return
        if event.key == 't':
            self.showverts = not self.showverts
            self.line.set_visible(self.showverts)
            if not self.showverts:
                self._ind = None

        self.canvas.draw()

    def motion_notify_callback(self, event):
        'on mouse movement'
        if not self.showverts:
            return
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata

        self.vertices[self._ind] = (x, y)
        vertices = np.asarray(self.vertices)
        self.line.set_data(vertices[:,0], vertices[:,1])
        # self.line.set_offsets(self.vertices)

        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)
        self.canvas.draw()

def main(args):
    pts = [(158, 257),
            (35, 11),
            (175, 20),
            (375, 20),
            (85, 115),
            (22, 32),
            (3, 5),
            (2, 5)]

    image = scipy.misc.imread(args['image'])
    interactor = Dotter(pts, image)
    interactor.ax.set_title('drag vertices to update path')
    # interactor.ax.set_xlim(-3, 4)
    # interactor.ax.set_ylim(-3, 4)

    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='hello, deep learning world!')
    parser.add_argument("--image", required=True, action="store", help="input image")
    args = vars(parser.parse_args())

    main(args)