import numpy as np
# from scipy.fftpack import fft

# 2D convolution using the convolution's FFT property
def conv2(a, b):
    ma, na = a.shape
    mb, nb = b.shape
    return np.fft.ifft2(np.fft.fft2(a, [2*ma-1, 2*na-1]) * np.fft.fft2(b, [2*mb-1, 2*nb-1]))


# compute a normalized 2D cross correlation using convolutions
# this will give the same output as matlab, albeit in row-major order
def normxcorr2(b, a):
    c = conv2(a, np.flipud(np.fliplr(b)))
    a = conv2(a**2, np.ones(b.shape))
    b = sum(b.flatten()**2)
    c = c/np.sqrt(a*b)
    return c
