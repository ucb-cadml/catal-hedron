import numpy as np
import matplotlib.pyplot as plt
import argparse
import scipy.misc

class ParticleModifier(object):
    """
    An path editor.

    Key-bindings

      't' toggle vertex markers on and off.  When vertex markers are on,
          you can move them, delete them


    """

    epsilon = 5  # max pixel distance to count as a vertex hit

    def __init__(self, ax, vertices=None):

        self.ax = ax
        if vertices is None:    self.vertices = np.zeros((0, 2), dtype=np.float)
        else:                   self.vertices = vertices
        self.h1, = self.ax.plot(self.vertices[:, 0], self.vertices[:, 1], marker='o', c='C0', linestyle='')
        self._ind = None

        canvas = self.ax.figure.canvas
        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        if self.h1 is not None: self.ax.draw_artist(self.h1)
        # self.ax.draw_artist(self.h1)
        self.canvas.blit(self.ax.bbox)


    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'
        if self.vertices.shape[0] == 0: return None
        xyt = self.ax.transData.transform(self.vertices)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x)**2 + (yt - event.y)**2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None
        return ind

    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if event.inaxes is None:
            return
        self._ind = self.get_ind_under_point(event)

        if event.button == 3:   # right click
            if self._ind is None:
                x, y = event.xdata, event.ydata
                self.vertices = np.concatenate([self.vertices, [[x, y]]], axis=0)
            else:
                self.vertices = np.delete(self.vertices, self._ind, 0)

            self.h1.set_data(self.vertices[:, 0], self.vertices[:, 1])
            print("pressed", len(self.vertices))

            self.canvas.restore_region(self.background)
            if self.h1 is not None: self.ax.draw_artist(self.h1)
            self.canvas.blit(self.ax.bbox)
            self.canvas.draw()

    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if event.button != 1:
            return
        self._ind = None

    def key_press_callback(self, event):
        'whenever a key is pressed'
        if not event.inaxes:
            return
        # if event.key == 't':
        self.canvas.draw()

    def motion_notify_callback(self, event):
        'on mouse movement'
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata

        self.vertices[self._ind, 0] = x
        self.vertices[self._ind, 1] = y

        self.h1.set_data(self.vertices[:, 0], self.vertices[:, 1])

        self.canvas.restore_region(self.background)
        if self.h1 is not None: self.ax.draw_artist(self.h1)
        self.canvas.blit(self.ax.bbox)
        self.canvas.draw()


def main(args):
    image = scipy.misc.imread(args.image)
    fig, ax = plt.subplots()
    ax.imshow(image)
    interactor = ParticleModifier(ax)
    ax.set_title('drag vertices to update path')
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='hello, deep learning world!')
    parser.add_argument("--image", required=True, action="store", help="input image")
    args = parser.parse_args()
    main(args)