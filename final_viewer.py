import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse
import scipy.misc
import math
from helpers import *


class FinalViewer(object):
    """
    An path editor.

    Key-bindings

      't' toggle vertex markers on and off.  When vertex markers are on,
          you can move them, delete them


    """

    epsilon = 5  # max pixel distance to count as a vertex hit

    def __init__(self, ax, image, vertices, neighbors, direction=[0, 2]):

        assert(direction in [[0, 2], [1, 3]])
        self.ax = ax
        self.vertices = vertices
        self.image = image
        self.G, self.ind13, self.ind24, self.GG = build_graph(self.vertices, neighbors)
        self.GG = self.GG.astype(np.int)
        self.h1, = self.ax.plot(self.vertices[:, 0], self.vertices[:, 1], marker='o', c='C0', linestyle='')
        self._ind = None
        self.colormap = cm.jet

        # this viewer shows lines between [0, 2] neighbors or [1, 3] neighbors
        self.direction = direction

        canvas = self.ax.figure.canvas
        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas
        self.hs = []
        self.draw()

    def draw(self):
        copy = self.hs[:]
        for h in copy:
            h.remove()
            self.hs.remove(h)

        nodes = self.G.nodes()
        pairs = []
        angles = []
        for ni in nodes:
            vi = self.vertices[ni]
            nj2 = self.GG[ni, self.direction]
            for nj in nj2:
                if nj != -1:
                    vj = self.vertices[nj]
                    angle = vj - vi
                    angle = math.atan(angle[1]/angle[0])
                    pairs.append([min(ni, nj), max(ni, nj)])
                    angles.append(angle)

        angles = np.array(angles)
        angles = (angles - np.min(angles)) / (np.max(angles) - np.min(angles))

        self.ax.imshow(self.image)
        for pair, a in zip(pairs, angles):
            p1 = self.vertices[pair[0]]
            p2 = self.vertices[pair[1]]
            h, = self.ax.plot([p1[0], p2[0]], [p1[1], p2[1]],
                              color=self.colormap(a),
                              linewidth=2)
            self.hs.append(h)
        self.pairs = pairs
        self.angles = angles

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        # if self.h1 is not None: self.ax.draw_artist(self.h1)
        # self.ax.draw_artist(self.h1)
        self.canvas.blit(self.ax.bbox)

    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'
        if self.vertices.shape[0] == 0: return None
        xyt = self.ax.transData.transform(self.vertices)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x)**2 + (yt - event.y)**2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None
        return ind

    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if event.inaxes is None:
            return
        self._ind = self.get_ind_under_point(event)
        self.canvas.blit(self.ax.bbox)
        self.canvas.draw()

    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if event.button != 1:
            return
        self._ind = None

        self.draw()
        # self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.h1)
        self.canvas.blit(self.ax.bbox)
        self.canvas.draw()


    def key_press_callback(self, event):
        'whenever a key is pressed'
        if not event.inaxes:
            return
        # if event.key == 't':
        self.canvas.draw()

    def motion_notify_callback(self, event):
        'on mouse movement'
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata

        self.vertices[self._ind, 0] = x
        self.vertices[self._ind, 1] = y

        # self.draw()
        self.h1.set_data(self.vertices[:, 0], self.vertices[:, 1])
        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.h1)
        self.canvas.blit(self.ax.bbox)
        self.canvas.draw()


def main(args):
    image = scipy.misc.imread(args.image)
    fig, ax = plt.subplots()
    ax.imshow(image)
    interactor = ParticleModifier(ax)
    ax.set_title('drag vertices to update path')
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='hello, deep learning world!')
    parser.add_argument("--image", required=True, action="store", help="input image")
    args = parser.parse_args()
    main(args)