This is an assistant tool to detect atom particles for chemical catalyst research. So far, since these codes are mainly developed for private research and not for open source purpose, we don't manage/support different system settings nor provide enough documentation. 

To use these codes, it would be betgter to contact:

- Paul Kwon, young@berkeley.edu
- Myunghwan Oh, mhoh@berkeley.edu
- Sara McMains, mcmains@berkeley.edu

# Requirement

- Ubuntu 16.4
- Python 3.5
- Install 

```
$ sudo apt-get install python-tk
$ pip install numpy scipy matplotlib                            
```

# How to run

1. Launch particle detector

```
$ python main.py --image images/2.jpg
```

You'll see the menu like this.

![menu](screenshots/fig0-menu.png)

2. Select reference points.

![menu](screenshots/fig0-selection.png)

3. Select mask.

![menu](screenshots/fig1-mask.png)

4. Do detection.

![menu](screenshots/fig2-detection.png)

5. Visualize the connections of two directions.

![menu](screenshots/fig6-diag1.png)
![menu](screenshots/fig7-diag2.png)

6. Visualize final result. 

![menu](screenshots/fig8-final.png)
