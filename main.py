import os
import pygame
import sys
import pickle
import math
import argparse
import scipy.misc
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
import seaborn as sns
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from path_patch import PolygonInteractor
from reference import ReferenceSelector
from dotter import ParticleModifier
from final_viewer import FinalViewer
from helpers import *
from tqdm import tqdm


pygame.init()
BLACK = (0, 0, 0)
WIDTH = 500
HEIGHT = 500
screen = pygame.display.set_mode((WIDTH, HEIGHT), 0, 32)



class CatalHedron():

    def __init__(self, args):
        self.file = args.input
        self.outf = ''.join(os.path.splitext(self.file)[:-1]+('.pickle',))
        self.image = scipy.misc.imread(self.file)
        height = self.image.shape[0]
        width = self.image.shape[1]

        self.reference = None
        self.locations = None
        # self.mask_xs_ = [0.2*width, 0.8*width, 0.8*width, 0.2*width]
        # self.mask_ys_ = [0.2*height, 0.2*height, 0.8*height, 0.8*height]
        # self.mask = [(x, y) for x, y in zip(self.mask_xs_, self.mask_ys_)]
        self.mask = np.array([[0.2*width, 0.2*height],
                              [0.8*width, 0.2*height],
                              [0.8*width, 0.8*height],
                              [0.2*width, 0.8*height]])
        self.load()
        self.message('initialized.')
        self.message('you can choose any menu.')

    def save(self):
        self.data = {"image": self.image,
                     "reference": self.reference,
                     "locations": self.locations,
                     "mask": self.mask}

        with open(self.outf, 'wb') as pf:
            pickle.dump(self.data, pf)

    def load(self):
        if os.path.isfile(self.outf):
            with open(self.outf, 'rb') as pf:
                self.data = pickle.load(pf)

            if "image" in self.data: self.image = self.data["image"]
            if "reference" in self.data: self.reference = self.data["reference"]
            if "locations" in self.data: self.locations = self.data["locations"]
            if "mask" in self.data: self.mask = self.data["mask"]

    def message(self, string):
        print (string)

    def select_reference(self):
        fig, ax = plt.subplots()
        ax.imshow(self.image)
        ax.set_title('Click and drag a point to move it')
        selector = ReferenceSelector(ax, self.reference)
        plt.show()
        self.reference = selector.vertices
        self.save()
        self.message('you can choose any menu.')

    def select_mask(self):
        poly = Polygon(self.mask, animated=True, alpha=0.6)
        fig, ax = plt.subplots()
        ax.imshow(self.image)
        ax.add_patch(poly)
        selector = PolygonInteractor(ax, poly)
        ax.set_title('Click and drag a point to move it')
        plt.show()
        self.mask = selector.poly.xy
        # self.mask_xs_ = list(selector.poly.xy[:, 0])
        # self.mask_ys_ = list(selector.poly.xy[:, 1])
        # self.mask = [(x, y) for x, y in zip(self.mask_xs_, self.mask_ys_)]
        self.save()
        self.message('you can choose any menu.')

    def process_detection(self):
        print('detection start...')
        self.load()
        if self.reference is None or self.reference.shape[0] != 5:
            self.message('Push the key 1, and select 5 reference points.')
            return
        if len(self.mask) < 3:
            self.message('Push the key 2, and select an area of interest.')
            return

        self.message("calculating...")
        maxima, pos, G, curves13, curves24, locs = \
            get_me_atoms(self.image, self.reference, self.mask)

        self.locations = locs
        self.message("calculating done.")
        self.save()

        ## visualize
        plt.figure(figsize=(10, 10))
        gs = gridspec.GridSpec(2, 2)
        ax1 = plt.subplot(gs[0, 0])
        ax1.imshow(self.image)
        ax1.plot(maxima[:, 0], maxima[:, 1], 'rx')
        ax1.set_title('first local maxima')

        ax2 = plt.subplot(gs[0, 1])
        ax2.imshow(self.image)
        ax2.plot(pos[:, 0], pos[:, 1], 'rx')
        ax2.set_title('local maxima in the mask')

        ax3 = plt.subplot(gs[1, 0])
        ax3.imshow(self.image)
        nx.draw_networkx(G, pos, node_size=4, font_size=6)
        ax3.set_title('graph network')

        ax4 = plt.subplot(gs[1, 1])
        ax4.imshow(self.image)
        draw_curves(curves13)
        draw_curves(curves24)
        ax4.plot(locs[:, 0], locs[:, 1], 'go')
        ax4.set_title('fitted curves and intersections')
        plt.show()
        self.message('detection done.  ')
        self.message('you can choose any menu.')

    def modify_detection(self):
        print('you can modify ...')
        # self.load()
        fig, ax = plt.subplots()
        ax.imshow(self.image)
        ax.set_title('Click and drag a point to move it')

        print("before modification", len(self.locations))
        selector = ParticleModifier(ax, self.locations)
        plt.show()

        self.locations = selector.vertices
        print("after modification", len(self.locations))

        from helpers import build_graph, get_curve, get_intersections
        pos = self.locations
        reference = self.reference
        mask = self.mask

        ref_ind = np.argmin(np.linalg.norm(pos - reference[0, :], axis=1))
        ## we do this twice, since we want to get rid of outliers,
        ## which indroduce new node numbering.
        neighbors = reference[1:, :] - reference[0, :]
        G, ind13, ind24, _ = build_graph(pos, neighbors, ref_ind)
        pos = pos[G.nodes()]
        G, ind13, ind24, _ = build_graph(pos, neighbors, ref_ind)


        ## curve fitting and intersections
        curves13 = get_curve(ind13, pos, mask)
        curves24 = get_curve(ind24, pos, mask)
        locs = get_intersections(curves13, curves24)
        locs = np.concatenate(locs, axis=0)
        self.locations = locs

        print("after modification", len(self.locations))

        self.save()
        self.message('modification done.')
        self.message('you can choose any menu.')

    def do_topology02(self):
        print('visualizing graph start...')
        self.load()
        assert (self.locations is not None and len(self.locations) > 10)

        fig, ax = plt.subplots()
        # ax.imshow(self.image)
        # ax.set_title('Drag a point to move it')
        neighbors = self.reference[1:, :] - self.reference[0, :]
        selector = FinalViewer(ax, self.image, self.locations, neighbors, direction=[0, 2])
        plt.show()
        self.locations = selector.vertices
        self.message('visualizing graph done.')
        self.message('you can choose any menu.')

    def do_topology13(self):
        print('visualizing graph start...')
        self.load()
        assert (self.locations is not None and len(self.locations) > 10)

        fig, ax = plt.subplots()
        # ax.imshow(self.image)
        # ax.set_title('Drag a point to move it')
        neighbors = self.reference[1:, :] - self.reference[0, :]
        selector = FinalViewer(ax, self.image, self.locations, neighbors, direction=[1, 3])
        plt.show()
        self.locations = selector.vertices
        self.message('visualizing graph done.')
        self.message('you can choose any menu.')

    def draw_diamonds(self, write_text=False):

        neighbors = self.reference[1:, :] - self.reference[0, :]
        G, ind13, ind24, GG = build_graph(self.locations, neighbors)
        GG = GG.astype(np.int)
        nodes = G.nodes()

        fig, ax = plt.subplots(figsize=(11, 10))
        # image = scipy.misc.imread('/home/phantom/projects/catal-hedron/images/2.jpg')
        # self.image = scipy.misc.imresize(image, [1257, 1257])
        plt.imshow(self.image)
        diamonds = []
        for ni in nodes:
            ni0 = GG[ni, 0]
            if ni0 is None: continue
            ni1 = GG[ni0, 1]
            if ni1 is None: continue
            ni2 = GG[ni1, 2]
            if ni2 is None: continue
            ni3 = GG[ni2, 3]
            if ni3 != ni: continue
            pts = self.locations[[ni, ni0, ni1, ni2, ni3], :]
            diamonds.append(pts)

        def aspect(pts):
            d1 = np.linalg.norm(pts[2]-pts[0])
            d2 = np.linalg.norm(pts[3]-pts[1])
            ap = d1 / d2
            # ap = max([ap / 1.08, 1.08 / ap])
            # ap = max([ap / 1.16, 1.16 / ap])
            # ap = max([d1, d2])/min([d1, d2])
            # ap = np.clip(ap, 1, 1.16)
            # ap /= 1.16
            return ap
            # return (max([d1, d2])-min([d1, d2])) / max([d1, d2])
            # return max([ap/1.15, 1.15/ap])
            # return max([d1, d2])/min([d1, d2])
        aspects = [aspect(pts) for pts in diamonds]
        aspects = np.array(aspects)

        patches = []
        for pts, cl in zip(diamonds, aspects):
            # plt.plot(pts[:, 0], pts[:, 1])
            polygon = Polygon(pts, True)
            patches.append(polygon)

        # move most populated bin to center
        bins = np.histogram(aspects, density=True, bins=np.linspace(0.75, 1.333, 50))
        bin_centers = (bins[1][1:] + bins[1][:-1])/2
        bin_max = bin_centers[np.argmax(bins[0])]
        aspects /= bin_max

        make_symetric = True
        if make_symetric:
            # inverse if aspects <= 1
            aspects[aspects < 1.] = 1. / aspects[aspects < 1.]
            asp_min, asp_max = (1.0, 1.25)
        else:
            asp_min, asp_max = (0.75, 1.25)

        histf = ''.join(os.path.splitext(self.file)[:-1]+('_hist.npy',))
        np.save(histf, aspects)

        # sns.set(style="white")
        p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4, edgecolor=None)
        p.set_array(aspects)
        # p.set_clim([0.75, 1.33])
        p.set_clim([asp_min, asp_max])
        ax.add_collection(p)

        if write_text:
            for pts, asp in zip(diamonds, aspects):
                ctr = np.mean(pts[:4], 0)
                plt.text(ctr[0], ctr[1], '%.2f' % asp, horizontalalignment='center',
                         verticalalignment='center',  fontsize=7)
            ax_min = np.min(np.concatenate(diamonds, 0), 0)
            ax_max = np.max(np.concatenate(diamonds, 0), 0)
            ax.set_xlim(ax_min[0] - 10, ax_max[0] + 10)
            ax.set_ylim(ax_min[1] - 10, ax_max[1] + 10)
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ax.invert_yaxis()

        ax.grid(False)
        plt.colorbar(p)
        plt.savefig(os.path.splitext(self.file)[0] + '_c.png', bbox_inches='tight')
        plt.show()

        sns.set(style="darkgrid")
        plt.hist(aspects, density=True, bins=np.linspace(0.75, 1.333, 100))  # arguments are passed to np.histogram
        plt.title("Histogram of aspect ratios")
        plt.grid(True)
        plt.xlim([asp_min, asp_max])
        plt.savefig(os.path.splitext(self.file)[0] + '_s.png', bbox_inches='tight')
        plt.show()

        return

    def do_fitting(self):
        neighbors = self.reference[1:, :] - self.reference[0, :]
        G, ind13, ind24, GG = build_graph(self.locations, neighbors)
        GG = GG.astype(np.int)
        nodes = G.nodes()

        from scipy.optimize import curve_fit
        from scipy.spatial.distance import cdist

        def func(x, a, b, c, d):
            return a * x**3 + b * x**2 + c * x + d
            # return a * x**2 + b * x + c
            # return a * np.exp(-b * x) + c

        def fit(indices, degree=2):
            """
            - do the curve fitting.
            - generate more finely sampled points on the fitted curves.
            - return all the sampled points
            """
            pts = {}
            for i in tqdm(range(np.min(indices), np.max(indices)+1)):
                xs = self.locations[indices == i, 0]
                ys = self.locations[indices == i, 1]

                if len(xs) <= 3: continue

                deltax = np.max(xs) - np.min(xs)
                deltay = np.max(ys) - np.min(ys)
                # plt.plot(xs, ys, 'o')

                if deltay > deltax:
                    change_axis = True
                    t = xs; xs = ys; ys = t
                else:
                    change_axis = False

                ii = np.argsort(xs)
                # xs = np.sort(xs)
                xs = xs[ii]
                ys = ys[ii]

                ii_back = np.zeros_like(ii)
                for k, iii in enumerate(ii):
                    ii_back[iii] = k
                # import pdb; pdb.set_trace()

                popt, pcov = curve_fit(func, xs, ys)
                # plt.plot(xs, func(xs, *popt), 'x')
                # plt.plot(xs, func(xs, *popt), 'r-',
                #          label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
                # plt.show()

                # ys = func(xs, *popt)
                nxs = np.linspace(np.min(xs), np.max(xs),
                                  int((np.max(xs) - np.min(xs)) * 1.5))
                nys = func(nxs, *popt)

                if change_axis:
                    t = xs; xs = ys; ys = t
                    t = nxs; nxs = nys; nys = t

                pts[i] = np.vstack([nxs, nys]).T
                # plt.plot(nxs, nys, 'x')
                # plt.show()
                # self.locations[indices == i, 0] = xs[ii_back]
                # self.locations[indices == i, 1] = ys[ii_back]

            return pts

        pts1 = fit(np.array(ind13))
        pts2 = fit(np.array(ind24))

        locs = []
        for u in pts1:
            for v in pts2:
                D = cdist(pts1[u], pts2[v])
                if np.min(D) > 3: continue
                pos = np.where(D == np.min(D))
                p = (pts1[u][pos[0], :] + pts2[v][pos[1], :]) * 0.5
                locs.append(p)

        self.locations = np.concatenate(locs)

        plt.plot(self.locations[:, 0], self.locations[:, 1], 'x')
        plt.show()

        # find nearest neighbors of original particles
        # from the finely-sampled points.








def main(args):
    catalhedron = CatalHedron(args)

    myfont = pygame.font.SysFont('Ubuntu', 20)
    text = myfont.render('Welcome to Catal-Hedron', 1, (255, 255, 0))

    instructions_text = ["key 1: Select reference points",
                         "key 2: Select mask",
                         "key 3: Do detection",
                         "key 4: Modify ",
                         "key 5: Draw edges (0-2)",
                         "key 6: Draw edges (1-3)",
                         "key 7: Draw diamonds",
                         "key s: Save",
                         "key q: Quit"]

    base_point = (30, 50)

    instructions = [myfont.render(t, 1, (0, 200, 0)) for t in instructions_text]

    # text1 = myfont.render('key 1: Select reference points', 1, (0, 255, 0))


    while True:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    catalhedron.select_reference()
                if event.key == pygame.K_2:
                    catalhedron.select_mask()
                if event.key == pygame.K_3:
                    catalhedron.process_detection()
                if event.key == pygame.K_4:
                    catalhedron.modify_detection()
                if event.key == pygame.K_5:
                    catalhedron.do_topology02()
                if event.key == pygame.K_6:
                    catalhedron.do_topology13()
                if event.key == pygame.K_7:
                    catalhedron.draw_diamonds()
                if event.key == pygame.K_8:
                    catalhedron.do_fitting()
                if event.key == pygame.K_s:
                    catalhedron.save()
                if event.key == pygame.K_q:
                    sys.exit()

        screen.fill(BLACK)
        screen.blit(text, (10, 10))
        for i, instruction in enumerate(instructions):
            screen.blit(instruction, (base_point[0], base_point[1] + i * 25))
        pygame.display.flip()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='hello, chemistry!')
    parser.add_argument("--input", required=True, help="input image path")
    args = parser.parse_args()

    main(args)