import numpy as np
from scipy.misc import face
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots
# from norm_xcorr import TemplateMatch

from norm_xcorr2 import normxcorr2

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

image = face()

image_ = rgb2gray(image)
template = image_[300:350, 550:650]

xx = normxcorr2(template, image_)
import pdb; pdb.set_trace()
# ncc, ssd = TM(image)

# TM = TemplateMatch(template, method='both')
# ncc, ssd = TM(image)
# nccloc = np.nonzero(ncc == ncc.max())
# ssdloc = np.nonzero(ssd == ssd.min())

fig, [[ax1, ax2], [ax3, ax4]] = subplots(2, 2, num='ND Template Search')
ax1.imshow(image, interpolation='nearest')
ax1.set_title('Search image')
ax2.imshow(template, interpolation='nearest')
ax2.set_title('Template')
plt.show()
# ax3.hold(True)
# ax3.imshow(ncc, interpolation='nearest')
# ax3.plot(nccloc[1], nccloc[0],'w+')
# ax3.set_title('Normalized cross-correlation')
# ax4.hold(True)
# ax4.imshow(ssd, interpolation='nearest')
# ax4.plot(ssdloc[1], ssdloc[0],'w+')
# ax4.set_title('Sum of squared differences')
# fig.tight_layout()
# fig.canvas.draw()
