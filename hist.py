import glob
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# sns.set()
# sinplot()

sns.set(style="darkgrid")
sns.set_palette("husl")
histfs = glob.glob('images/*.npy')
histfs = sorted(histfs)

fig = plt.figure()
for histf in histfs[:1]:
    aspects = np.load(histf)
    l = os.path.splitext(os.path.basename(histf))[0]
    sns.distplot(aspects, np.linspace(0.75, 1.333, 50), kde=False, hist=True, norm_hist=True, label=l)
    # plt.hist(aspects, density=True, bins=np.linspace(0.75, 1.333, 50),
             # label=l, histtype='step', alpha=1.0,
             # color=sns.color_palette()[0])
    # xx = np.histogram(aspects, density=True, bins=np.linspace(0.75, 1.333, 50))
    # plt.plot((xx[1][1:] + xx[1][:-1])/2, xx[0], label=l)

plt.title("Histogram of aspect ratios")
plt.grid(True)
plt.xlim([0.65, 1.35])
plt.legend()
plt.show()