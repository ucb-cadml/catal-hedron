import os
import glob
import numpy as np

files = glob.glob('images/*.npy')
for file in files:
    aspects = np.load(file)
    with open(file.replace('.npy', '.txt'), 'w') as f:
        f.write(' '.join(['%.4f' % v for v in aspects]))
        f.write('\n')
