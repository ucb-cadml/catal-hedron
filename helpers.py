import cv2
import numpy as np
import networkx as nx
from scipy.spatial.distance import cdist
import scipy.ndimage.filters as filters
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon as gPolygon
import matplotlib.pyplot as plt


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])

def get_me_atoms(image, reference, mask, curve_fitting=False):
    maxima = detection(reference, image, neighborhood_size=10)
    polygon = gPolygon(mask)
    within_check = [polygon.contains(Point(l[0], l[1])) for l in maxima]
    pos = maxima[within_check]

    ref_ind = np.argmin(np.linalg.norm(pos - reference[0, :], axis=1))
    ## we do this twice, since we want to get rid of outliers,
    ## which indroduce new node numbering.
    neighbors = reference[1:, :] - reference[0, :]
    G, ind13, ind24, _ = build_graph(pos, neighbors, ref_ind)
    pos = pos[G.nodes()]
    ref_ind = np.argmin(np.linalg.norm(pos - reference[0, :], axis=1))
    G, ind13, ind24, _ = build_graph(pos, neighbors, ref_ind)


    ## curve fitting and intersections
    curves13 = get_curve(ind13, pos, mask)
    curves24 = get_curve(ind24, pos, mask)
    locs = get_intersections(curves13, curves24)
    locs = np.concatenate(locs, axis=0)
    return maxima, pos, G, curves13, curves24, locs


def detection(reference, image, pad=20, neighborhood_size=20, alpha=0.8):
    main = reference[0, :]
    xmin = np.min(reference[:, 0])
    xmax = np.max(reference[:, 0])
    ymin = np.min(reference[:, 1])
    ymax = np.max(reference[:, 1])
    xradius = min([reference[0, 0] - xmin, xmax - reference[0, 0]])
    yradius = min([reference[0, 1] - ymin, ymax - reference[0, 1]])

    # crop template
    image_gray = rgb2gray(image).astype(np.uint8)
    template = image_gray[int(ymin)-pad:int(ymax)+pad, int(xmin)-pad:int(xmax)+pad]
    relative_pos = main - np.array([xmin-pad, ymin-pad])

    # template matching (with padding)
    tr, tc = template.shape
    image_gray_ = np.pad(image_gray, ((tr, tr), (tc, tc)), 'reflect')
    response = cv2.matchTemplate(image_gray_, template, cv2.TM_CCOEFF_NORMED)

    # local maxima
    data_max = filters.maximum_filter(response, neighborhood_size)
    threshold = alpha * np.min(response) + (1 - alpha) * np.max(response)
    maxima = np.logical_and(response == data_max, response > threshold)
    r, c = np.nonzero(maxima)
    loc_max = np.array([[cv, rv] for rv, cv in zip(r, c)])
    pos = loc_max + relative_pos - np.array([[tc, tr]])
    plt.imshow(image_gray); plt.plot(pos[:,0], pos[:,1], 'rx'); plt.show()
    return pos


def build_graph(pos, reference_nn4, ref_ind=0):
    assert(reference_nn4.shape[0] == 4)
    G = nx.Graph()

    typical_dist = np.sqrt(np.sum(reference_nn4 ** 2, 1))
    n = pos.shape[0]
    ind13 = [None] * n
    ind24 = [None] * n

    # try:
    checked = []
    queue = [ref_ind]
    ind13[queue[0]] = 0
    ind24[queue[0]] = 0
    GG = np.zeros((n, 4))
    # except:

    while len(queue) > 0:
        q = queue[0]
        queue = queue[1:]
        checked.append(q)
        ref_nn4 = reference_nn4 + pos[q]

        d = cdist(ref_nn4, pos, 'euclidean')     # d is 4 x n
        nn4 = np.argmin(d, 1)
        nd4 = np.min(d, 1)

        invalid = np.logical_or(nd4 > 0.3 * typical_dist, nn4 == q)  #  no self edge, no too much deviation

        if not invalid[0]: ind13[nn4[0]] = ind13[q]-1; ind24[nn4[0]] = ind24[q]
        if not invalid[1]: ind13[nn4[1]] = ind13[q];   ind24[nn4[1]] = ind24[q]-1
        if not invalid[2]: ind13[nn4[2]] = ind13[q]+1; ind24[nn4[2]] = ind24[q]
        if not invalid[3]: ind13[nn4[3]] = ind13[q];   ind24[nn4[3]] = ind24[q]+1

        nn4[invalid] = -1
        GG[q, :] = nn4                       # want to preserve reference index
        nn4 = nn4[np.logical_not(invalid)]   # now remove

        for i in range(len(nn4)):
            n = nn4[i]
            try: G.add_edge(q, n)
            except: pass

        neq = [idx for idx in nn4 if idx not in queue and idx not in checked]
        queue += neq
    return G, ind13, ind24, GG



def get_poly_line2(refx, refy):

    resolution = 0.5
    # xs = np.arange(np.min(refx), np.max(refx), resolution)
    xs = np.arange(np.min(refx)-10, np.max(refx)+10, resolution)

    if refx.shape[0] < 4:
        xs = []; ys = []
        return xs, ys
    # elif refx.shape[0] < 10:
    #     deg = 1
    #     z = np.poly1d(np.polyfit(refx, refy, deg))
    #     ys = [z(x) for x in xs]
    #     # deg = 3
    #     # z2 = np.poly1d(np.polyfit(refx, refy, deg))
    #     # deg = 4
    #     # z3 = np.poly1d(np.polyfit(refx, refy, deg))
    #     # ys2 = [z2(x) for x in xs]
    #     # ys3 = [z3(x) for x in xs]
    #     # ys = [0.6 * y2 + 0.4 * y3 for y2, y3 in zip(ys2, ys3)]
    else:
        deg = 2
        z1 = np.poly1d(np.polyfit(refx, refy, deg))
        deg = 4
        z2 = np.poly1d(np.polyfit(refx, refy, deg))
        deg = 5
        z3 = np.poly1d(np.polyfit(refx, refy, deg))
        ys1 = [z1(x) for x in xs]
        ys2 = [z2(x) for x in xs]
        ys3 = [z3(x) for x in xs]
        ys = [0.9 * y1 + 0.1 * y2 + 0.0 * y3 for y1, y2, y3 in zip(ys1, ys2, ys3)]
    # else:
    #     from scipy.interpolate import interp1d
    #     z = interp1d(refx, refy)
    #     ys = [z(x) for x in xs]
    return xs, ys


# def get_poly_line2(refx, refy):

#     resolution = 0.5
#     xs = np.arange(np.min(refx), np.max(refx), resolution)

#     if refx.shape[0] < 4:
#         xs = []; ys = []
#         return xs, ys
#     elif refx.shape[0] < 10:
#         # deg = 1
#         # z = np.poly1d(np.polyfit(refx, refy, deg))
#         # ys = [z(x) for x in xs]
#         deg = 3
#         z2 = np.poly1d(np.polyfit(refx, refy, deg))
#         deg = 4
#         z3 = np.poly1d(np.polyfit(refx, refy, deg))
#         ys2 = [z2(x) for x in xs]
#         ys3 = [z3(x) for x in xs]
#         ys = [0.6 * y2 + 0.4 * y3 for y2, y3 in zip(ys2, ys3)]
#     else:
#         deg = 7
#         z2 = np.poly1d(np.polyfit(refx, refy, deg))
#         deg = 8
#         z3 = np.poly1d(np.polyfit(refx, refy, deg))
#         ys2 = [z2(x) for x in xs]
#         ys3 = [z3(x) for x in xs]
#         ys = [0.6 * y2 + 0.4 * y3 for y2, y3 in zip(ys2, ys3)]
#     # else:
#     #     from scipy.interpolate import interp1d
#     #     z = interp1d(refx, refy)
#     #     ys = [z(x) for x in xs]
#     return xs, ys


def get_curve(axis, locations, mask):

    axis = np.array(axis)
    curves = {}
    for i in range(np.min(axis), np.max(axis)+1):
        x = locations[axis==i, 0]
        y = locations[axis==i, 1]

        # line could be horizontal and vertical.
        # so choose "main axis" as the axis chaginging more greatly.
        deltax = np.max(x) - np.min(x);
        deltay = np.max(y) - np.min(y);

        if deltay > deltax:
            change_axis = True;
            t=x; x=y; y=t;
        else:
            change_axis = False;

        newx, newy = get_poly_line2(x, y)
        # from scipy.interpolate import interp1d

        # if len(x) < 2: continue
        # f = interp1d(x, y)
        # newx = np.arange(np.min(x), np.max(x), resolution)
        # newy = f(newx)

        if change_axis:
            t=x; x=y; y=t;
            t=newx; newx=newy; newy=t;

        polygon = gPolygon(mask)
        within_check = [polygon.contains(Point(x, y)) for x, y in zip(newx, newy)]

        xs = [v for v, b in zip(newx, within_check) if b]
        ys = [v for v, b in zip(newy, within_check) if b]
        curve = np.array([[x, y] for x, y in zip(xs, ys)])
        if curve.shape[0] > 2:
            curves[i] = curve
    return curves

def draw_curves(curves):
    for i in curves:
        plt.plot(curves[i][:,0], curves[i][:,1])

def get_intersections(curves13, curves24):
    uvs = []
    locs = []
    for i in curves13:
        for j in curves24:
            if curves13[i].shape[0] < 2 or curves24[j].shape[0] < 2: continue
            distances = cdist(curves13[i], curves24[j], 'euclidean')
            min_distance = np.min(distances)
            if min_distance > 1.5: continue
            i13, j24 = np.where(distances == min_distance)
            uvs.append([i, j])
            locs.append(0.5 * (curves13[i][i13] + curves24[j][j24]))
    return locs
